/*global systemDictionary:true */
'use strict';

systemDictionary = {
	'rct adapter settings': {
		'en': 'Adapter settings for rct',
		'de': 'Adaptereinstellungen für rct',
		'ru': 'Настройки адаптера для rct',
		'pt': 'Configurações do adaptador para rct',
		'nl': 'Adapterinstellingen voor rct',
		'fr': "Paramètres d'adaptateur pour rct",
		'it': "Impostazioni dell'adattatore per rct",
		'es': 'Ajustes del adaptador para rct',
		'pl': 'Ustawienia adaptera dla rct',
		'zh-cn': 'rct的适配器设置'
	},
	'option1': {
		'en': 'option1',
		'de': 'Option 1',
		'ru': 'Опция 1',
		'pt': 'Opção 1',
		'nl': 'Optie 1',
		'fr': 'Option 1',
		'it': 'opzione 1',
		'es': 'Opción 1',
		'pl': 'opcja 1',
		'zh-cn': '选项1'
	},
	'option2': {
		'en': 'option2',
		'de': 'Option 2',
		'ru': 'option2',
		'pt': 'opção 2',
		'nl': 'Optie 2',
		'fr': 'Option 2',
		'it': 'opzione 2',
		'es': 'opcion 2',
		'pl': 'Opcja 2',
		'zh-cn': '选项2'
	}
};